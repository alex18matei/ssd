import random
import math

KEY_ROUNDS = 6
KEY_LENGTH = 128
KEY_SHIFT = 25
ROUNDS_NO = 8


def split_in_16_bits_blocks(key, blocks=8):
    blocks_16 = []
    for i in range(0, int(math.ceil(key.bit_length() / 16))):
        blocks_16.append(key & 0xFFFF)
        # print("{:016b}".format(blocks_16[i]), blocks_16[i])
        key = (key >> 16) | 0x0000

    return blocks_16[::-1]


def unify_16_bits_blocks(blocks):
    res = blocks[0]
    for i in range(1, len(blocks)):
        res = (res << 16) | blocks[i]

    return res


def cyclic_left_shift(key, shifts):
    return ((key << shifts) | (key >> KEY_LENGTH - shifts)) & (pow(2, 128) - 1)


def key_gen(key):
    result = []

    # print(split_in_16_bits_blocks(key))
    for rounds in range(0, 6):
        result.extend(split_in_16_bits_blocks(key))
        key = cyclic_left_shift(key, KEY_SHIFT)

    result.extend(split_in_16_bits_blocks(key)[:4])
    return result


def addModulo(x, y):
    return (x + y) % 0xFFFF


# 0 is treated as 2^ 16 to respect group properties
def mulModulo(x, y):
    z = (x * y) % 0x10001
    if z == 0x10000:
        return 0x0000
    return z


# Fermat's little th.
#  a^(m-1) = 1 (mod m)
# multiply both sides with a-1
#  a^(-1) = a ^ (m-2) (mod m)
def modInverse(x):
    if x == 0:
        return 0
    else:
        return pow(x, 0xFFFF, 0x10001)


# x + y = 0 mod m
# y = -x mod m
def addInverse(x):
    return (-x) % 0xFFFF


def encrypt(key_schedule, blocks):
    x1 = blocks[0]
    x2 = blocks[1]
    x3 = blocks[2]
    x4 = blocks[3]

    for round in range(ROUNDS_NO):
        index = round * 6
        x1 = mulModulo(x1, key_schedule[index])
        x2 = addModulo(x2, key_schedule[index + 1])
        x3 = addModulo(x3, key_schedule[index + 2])
        x4 = mulModulo(x4, key_schedule[index + 3])

        t0 = mulModulo(key_schedule[index + 4], x1 ^ x3)
        t1 = mulModulo(key_schedule[index + 5], addModulo(t0, x2 ^ x4))
        t2 = addModulo(t0, t1)

        x1 ^= t1
        x4 ^= t2
        a = x2 ^ t2
        x2 = x3 ^ t1
        x3 = a

    a = x2
    x1 = mulModulo(x1, key_schedule[-4])
    x2 = addModulo(x3, key_schedule[-3])
    x3 = addModulo(a, key_schedule[-2])
    x4 = mulModulo(x4, key_schedule[-1])

    return (x1, x2, x3, x4)

# http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.14.3451&rep=rep1&type=pdf
def invert_key(key_schedule):
    result = []
    result.append(modInverse(key_schedule[-4]))
    result.append(addInverse(key_schedule[-3]))
    result.append(addInverse(key_schedule[-2]))
    result.append(modInverse(key_schedule[-1]))
    result.append(key_schedule[-6])
    result.append(key_schedule[-5])

    for rounds in range(1, ROUNDS_NO):
        index = rounds * 6
        result.append(modInverse(key_schedule[-index - 4]))
        result.append(addInverse(key_schedule[-index - 2]))
        result.append(addInverse(key_schedule[-index - 3]))
        result.append(modInverse(key_schedule[-index - 1]))
        result.append(key_schedule[-index - 6])
        result.append(key_schedule[-index - 5])

    result.append(modInverse(key_schedule[0]))
    result.append(addInverse(key_schedule[1]))
    result.append(addInverse(key_schedule[2]))
    result.append(modInverse(key_schedule[3]))
    return result


def decrypt(key_schedule, blocks):
    inverted = invert_key(key_schedule)

    return encrypt(inverted, blocks)


def print_in_bytes(blocks):
    res = []
    for block in blocks:
        res.append(block >> 8)
        res.append(block & 0xFF)
    return res


def encrypt_idea(key_schedule, message):
    splitted_message = split_in_16_bits_blocks(message, 4)
    # print('message', print_in_bytes(splitted_message))
    return unify_16_bits_blocks(
        encrypt(key_schedule, splitted_message)
    )


def decrypt_idea(key_schedule, message):
    splitted_message = split_in_16_bits_blocks(message, 4)
    # print('message', print_in_bytes(splitted_message))
    return unify_16_bits_blocks(
        encrypt(invert_key(key_schedule), splitted_message)
    )


def test():
    pass

    # key = random.getrandbits(128)
    key = 213625554189589524912007485851402161191
    # print("key", key)
    # print("key in bits: {:0128b}".format(key))

    # message = random.getrandbits(64)
    message = 6930267949760019407

    key_schedule = key_gen(key)
    # print(len(key_schedule), key_schedule)

    encrypted_blocks = encrypt(key_schedule, split_in_16_bits_blocks(message))

    print('encrypted_blocks', print_in_bytes(encrypted_blocks))
    print('decrypted_blocks', print_in_bytes(decrypt(key_schedule, encrypted_blocks)))


if __name__ == "__main__":
    test()
