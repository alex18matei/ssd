import random
import idea
iv = 6930267949760019408

def encrypt_idea_cbc(blocks, key_schedule):

    print("blocks", blocks)
    encrypted_blocks = []
    encrypted_blocks.append(iv)
    for block in blocks:
        print("block", block)
        print("encrypted_blocks[-1]", encrypted_blocks[-1])
        precipher = block ^ encrypted_blocks[-1]
        print("precipher", precipher)
        encrypted_blocks.append(idea.encrypt_idea(key_schedule, precipher))
    return encrypted_blocks


def decrypt_idea_cbc(encrypted_blocks, key_schedule):

    decrypted_blocks = []
    for index in range(1, len(encrypted_blocks)):
        precipher = idea.decrypt_idea(key_schedule, encrypted_blocks[index])
        decrypted_blocks.append(precipher ^ encrypted_blocks[index - 1])

    return decrypted_blocks

def test():
    pass

    key = random.getrandbits(128)
    # key = 213625554189589524912007485851402161191

    message = [random.getrandbits(64) for i in range(0,5)]
    # message = [6930267949760019407, 6930267949760019408]

    key_schedule = idea.key_gen(key)
    # print(len(key_schedule), key_schedule)

    encrypted = encrypt_idea_cbc(message, key_schedule)
    print("encrypt", encrypted)
    print("decrypt", decrypt_idea_cbc(encrypted, key_schedule))


if __name__ == "__main__":
    test()
