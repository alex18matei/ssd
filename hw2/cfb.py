import random
import idea


# iv = 6930267949760019408


# def encrypt_idea_cfb(plaintext_blocks, key_schedule):
#     print("blocks", plaintext_blocks)
#     ciphertext_blocks = []
#     to_encrypt = iv
#     for block in plaintext_blocks:
#         precipher = idea.encrypt_idea(key_schedule, to_encrypt)
#         ciphertext_blocks.append(block ^ precipher)
#         to_encrypt = block
#
#     return ciphertext_blocks
#
#
# def decrypt_idea_cfb(ciphertext_blocks, key_schedule):
#     decrypted_blocks = []
#     to_encrypt = iv
#     for block in ciphertext_blocks:
#         precipher = idea.encrypt_idea(key_schedule, to_encrypt)
#         decrypted = block ^ precipher
#         # print(decrypted)
#         decrypted_blocks.append(decrypted)
#         to_encrypt = decrypted
#
#     return decrypted_blocks


# def unify_8_bits_blocks(blocks):
#     res = blocks[0]
#     for i in range(1, len(blocks)):
#         res = (res << 8) | blocks[i]
#
#     return res


iv = random.getrandbits(64)


def encrypt_idea_cfb(plaintext_blocks, key_schedule):
    print("blocks", plaintext_blocks)
    ciphertext_blocks = [iv]

    for block in plaintext_blocks:
        precipher = idea.encrypt_idea(key_schedule, ciphertext_blocks[-1])
        ciphertext_blocks.append(block ^ precipher)

    return ciphertext_blocks


def decrypt_idea_cfb(ciphertext_blocks, key_schedule):
    decrypted_blocks = []

    to_encrypt = iv
    for index in range(1, len(ciphertext_blocks)):
        precipher = idea.encrypt_idea(key_schedule, to_encrypt)
        decrypted = ciphertext_blocks[index] ^ precipher
        decrypted_blocks.append(decrypted)
        to_encrypt = ciphertext_blocks[index]

    return decrypted_blocks


def test():
    pass

    key = random.getrandbits(128)
    message = [random.getrandbits(64) for i in range(0, 5)]

    key_schedule = idea.key_gen(key)

    encrypted = encrypt_idea_cfb(message, key_schedule)
    print("encrypt", encrypted)
    print("decrypt", decrypt_idea_cfb(encrypted, key_schedule))

    # print(unify_8_bits_blocks(bytearray("InitializationVe".encode())))


if __name__ == "__main__":
    test()
