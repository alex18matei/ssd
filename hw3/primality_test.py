import time
import random
import millerrabin
import solovaystrassen


def testMillerRabin(numbers):
    counter = 0
    for i in range(0, numbers):
        number = random.getrandbits(32)
        isPrime = millerrabin.is_prime(number)
        if isPrime: counter += 1
        #     print("{} : {}".format(number, isPrime))
    return counter


def testSolovayStrassen(numbers):
    counter = 0
    for i in range(0, numbers):
        number = random.getrandbits(32)
        isPrime = solovaystrassen.is_prime(number)
        if isPrime: counter += 1
    return counter


def test(numbers, alg):
    start = time.time()
    if alg == "Miller-Rabin":
        counter = testMillerRabin(numbers)
    else:
        counter = testSolovayStrassen(numbers)
    end = time.time()
    print(numbers, "numbers", alg, end - start, "found", counter, "numbers")


if __name__ == "__main__":
    test(1000, "Miller-Rabin")
    test(10000, "Miller-Rabin")
    test(100000, "Miller-Rabin")

    test(1000, "SolovayStrassen")
    test(10000, "SolovayStrassen")
    test(100000, "SolovayStrassen")
