import random

ITERATIONS = 10


def calculateJacobian(a, n):
    if a == 0:
        return 0

    ans = 1
    if a < 0:
        a = -a
        if n % 4 == 3:
            ans = -ans
    if a == 1:
        return ans

    while a != 0:
        if a < 0:
            a = -a
            if n % 4 == 3:
                ans = -ans

        while a % 2 == 0:
            a = int(a / 2)
            if n % 8 == 3 or n % 8 == 5:
                ans = -ans

        a, n = n, a

        if a % 4 == 3 and n % 4 == 3:
            ans = -ans
        a = a % n

        if a > n / 2:
            a = a - n

    if n is 1:
        return ans
    return 0


def is_prime(p):
    if p < 2:
        return False
    if p != 2 and p % 2 == 0:
        return False

    for i in range(ITERATIONS):
        a = random.randint(2, p - 1)
        jacobian = (p + calculateJacobian(a, p)) % p
        mod = pow(a, int((p - 1) / 2), p) % p
        if (jacobian == 0) or (mod != jacobian):
            return False

    return True


def test():
    for i in range(10000, 20000):
        print("{} : {}".format(i, is_prime(i)))


if __name__ == "__main__":
    test()
