import random

ITERATIONS = 20


def is_composite(a, s, d, p):
    if pow(a, s, p) is 1:
        return False
    for i in range(d):
        if pow(a, 2 ** i * s, p) is p - 1:
            return False
    return True


def is_prime(p):
    if p < 2:
        return False
    if p is not 2 and p % 2 is 0:
        return False

    s = p - 1
    d = 0
    while s % 2 is 0:
        s = int(s / 2)
        d += 1

    for i in range(ITERATIONS):
        # a = random.randint(2, p - 1)
        # if is_composite(a, s, d, p):
        #     return False
        a = random.randint(2, p - 1)
        temp = s
        mod = pow(a, temp, p)
        while temp != p - 1 and mod != 1 and mod != p - 1:
            mod = pow(a, 2 ** i * s, p)
            temp *= 2
        if mod != p - 1 and temp % 2 == 0:
            return False
    return True


def test():
    for i in range(10, 20):
        print("{} : {}".format(i, is_prime(i)))


if __name__ == "__main__":
    test()
